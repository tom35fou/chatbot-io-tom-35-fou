export class Bot {
  name: string;
  avatar: string;
  commands: { [key: string]: string };

  constructor(name: string, avatar: string, commands: { [key: string]: string }) {
    this.name = name;
    this.avatar = avatar;
    this.commands = commands;
  }

  respond(message: string): string {
    for (const command in this.commands) {
      if (message.toLowerCase().includes(command)) {
        return this.commands[command];
      }
    }
    return "Je ne comprends pas cette commande.";
  }
}
