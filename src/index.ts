import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/styles.css';
import { Chat } from './components/Chat';
import { ContactList } from './components/ContactList';

document.addEventListener('DOMContentLoaded', () => {
  const app = document.getElementById('app');
  if (app) {
    const contactListContainer = document.createElement('div');
    contactListContainer.id = 'contact-list-container';
    const chatContainer = document.createElement('div');
    chatContainer.id = 'chat-container';
    app.appendChild(contactListContainer);
    app.appendChild(chatContainer);

    const chat = new Chat(chatContainer);
    chat.init();

    const contactList = new ContactList(contactListContainer, (selectedBot) => {
      chat.setBot(selectedBot);
    });
    contactList.render();
  }
});
