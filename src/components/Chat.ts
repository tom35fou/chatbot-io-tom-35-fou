import axios from 'axios';
import BlaguesAPI from 'blagues-api';
import { Bot } from '../bots/Bot';

export class Chat {
  private container: HTMLElement;

  private bot: Bot | null = null;

  constructor(container: HTMLElement) {
    this.container = container;
  }

  init() {
    this.render();
    this.bindEvents();
  }

  private render() {
    this.container.innerHTML = `
      <div class="chat-window d-none">
        <div id="selectedBot" class="mb-3"></div>
        <div id="messageList" class="message-list"></div>
        <div class="input-group">
          <input id="messageInput" type="text" class="form-control" placeholder="Écrivez un message" disabled>
          <button id="sendButton" class="btn btn-primary" disabled>Envoyer</button>
        </div>
      </div>
    `;
  }

  private bindEvents() {
    const sendButton = document.getElementById('sendButton');
    const messageInput = document.getElementById('messageInput') as HTMLInputElement;
    sendButton?.addEventListener('click', () => this.sendMessage(messageInput.value));
    messageInput?.addEventListener('keypress', (event) => {
      if (event.key === 'Enter') {
        this.sendMessage(messageInput.value);
      }
    });
  }

  public setBot(bot: Bot) {
    this.bot = bot;
    const chatWindow = document.querySelector('.chat-window') as HTMLElement;
    const selectedBot = document.getElementById('selectedBot') as HTMLElement;
    const messageInput = document.getElementById('messageInput') as HTMLInputElement;
    const sendButton = document.getElementById('sendButton') as HTMLButtonElement;
    const messageList = document.getElementById('messageList') as HTMLElement;

    if (chatWindow && selectedBot && messageInput && sendButton && messageList) {
      chatWindow.classList.remove('d-none');
      selectedBot.textContent = `Discussion avec ${bot.name}`;
      messageInput.disabled = false;
      sendButton.disabled = false;
      messageList.innerHTML = ''; // Reset message list on bot change
    }
  }

  private async sendMessage(message: string) {
    if (!message.trim() || !this.bot) return;

    // Ajout de la condition pour la commande "blague"
    if (message.toLowerCase() === 'blague' && this.bot) {
      const blagues = new BlaguesAPI('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiOTUxODU2NjUzMzE2MjcyMTM4IiwibGltaXQiOjEwMCwia2V5IjoiRkFxVzlvTjJKVVF3TldUSkZFaWFIcnNWS2FtaEozVXlhbzJCZlhsNXAyS2pxTTRGUHoiLCJjcmVhdGVkX2F0IjoiMjAyNC0wNi0xN1QxMzowODo0OCswMDowMCIsImlhdCI6MTcxODYyOTcyOH0.FhJTDiQoSNwC9NASTSFg42pSS9WxbangCU0enZTTmUo');
      const joke = await blagues.random();
      console.log(joke);

      this.addMessage(this.bot.name, joke.joke, 'right');
      this.addMessage(this.bot.name, joke.answer, 'left');
      return;
    }
    // Si ce n'est pas une commande spéciale, envoyer le message au bot
    this.addMessage('Vous', message, 'right');
    const response = await this.bot.respond(message);
    this.addMessage(this.bot.name, response, 'left');

    this.clearMessageInput();
  }

  private addMessage(sender: string, message: string, side: 'left' | 'right') {
    const messageList = document.getElementById('messageList');
    if (messageList) {
      this.addMessageToList(messageList, sender, message, side);
    }
  }

  private addMessageToList(messageList: HTMLElement, sender: string, message: string, side: 'left' | 'right') {
    const messageElement = document.createElement('div');
    messageElement.classList.add('message', side === 'right' ? 'text-end' : 'text-start');
    messageElement.innerHTML = `
      <div class="message-content">
        <div class="message-sender">${sender}</div>
        <div class="message-text">${message}</div>
      </div>
    `;
    messageList.appendChild(messageElement);
    messageList.scrollTop = messageList.scrollHeight;
  }

  private clearMessageInput() {
    const messageInput = document.getElementById('messageInput') as HTMLInputElement;
    if (messageInput) {
      messageInput.value = '';
    }
  }

  public async getJoke() {
    try {
      console.log('JPP');
      const response = await axios.get('https://api.blagues-api.fr/random');
      if (response.data && response.data.joke) {
        return response.data.joke;
      }
      return 'Je n\'ai pas pu trouver de blague pour le moment.';
    } catch (error) {
      console.error('Erreur lors de la récupération de la blague :', error);
      return 'Une erreur s\'est produite lors de la récupération de la blague.';
    }
  }
}
